CountAnything
=============
**[Project homepage](http://tapesoftware.net/countanything/)**

![Main window](https://tapesoftware.net/content/images/ec5088aba45b8fbbd8fa998f9da48989b88f74d0aa416d48e0af05b261e97f05.png)

This is a generic (Windows-only) counting application. It allows the user to have a counter, which can be incremented (and decremented and reset if necessary) by pressing a hotkey.

# Usage

Start the CountAnything application and you will see the above window. You can move it around by clicking and dragging anywhere in the window. Right-Click to open the context menu, and click “Configure…” to open the configuration dialog (described in the next section).

To include the counter in a live stream, use a software capture in OBS / whatever it is called in XSplit. You can use a color key to make the counter transparent.

# Configuration

The configuration dialog has three pages: _General_, _Display_ and _Hotkeys_. All changes you make in this dialog are saved automatically and will be loaded the next time you start CountAnything.

## General

![General config](https://tapesoftware.net/content/images/2020/01/2020_01_30_21_10_48_CountAnything_Configuration.png)

*   _Count to what_: Target value of the counter.

## Display

![Display config](https://tapesoftware.net/content/images/2020/01/ng.png)

*   _Not finished color:_ Color of the counter when it has not reached the target value.
*   _Finished color:_ Color of the counter when it has reached the target value.
*   _Background color:_ Background color of the counter.
*   _Font:_ Font used in the counter.
*   _Format:_ Text format of the counter (i.e. the `{0}/{1}` resulted in the `18/60` in the screenshot above)

## Hotkeys

![Hotkeys config](https://tapesoftware.net/content/images/2020/01/2020_01_30_21_13_47_CountAnything_Configuration.png)

*   _Double Tap Prevention:_ If a hotkey is pressed twice in the specified timeframe, it will only be registered the first time.
*   _Add/Subtract N:_ Up to two hotkeys for changing the counter by a custom amount.

The hotkeys are self-explanatory. Click inside the boxes and press the desired key combination. Click on the “Clear” button to remove the hotkey.
